<?php

namespace App\Controller;

use App\Entity\ContactMessage;
use App\Form\Type\BottomContactType;
use App\Form\Type\ContactType;
use App\Form\Type\SliderContactType;
use App\Services\ContactMailer;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class IndexController extends AbstractController
{
    public function index(Request $request, ContactMailer $contactMailer) : Response
    {
        $contactMessageSlider = new ContactMessage();
        $formSlider = $this->createForm(SliderContactType::class, $contactMessageSlider);
        $formSlider->handleRequest($request);

        $contactMessageBottom = new ContactMessage();
        $formBottom = $this->createForm(BottomContactType::class, $contactMessageBottom);
        $formBottom->handleRequest($request);
        if ($formSlider->isSubmitted()) {
            if ($formSlider->isValid()) {
                /** @var UploadedFile $file */
                $file = $formSlider['file']->getData();
                if ($file) {
                    $originalFilename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
                    // this is needed to safely include the file name as part of the URL
                    $safeFilename = transliterator_transliterate('Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()', $originalFilename);
                    $newFilename = $safeFilename . '-' . uniqid() . '.' . $file->guessExtension();

                    // Move the file to the directory where brochures are stored
                    try {
                        $file->move(
                            $this->getParameter('kernel.project_dir') . '/uploads/badania',
                            $newFilename
                        );
                    } catch (FileException $e) {}

                    $contactMessageSlider->setFilename($newFilename);
                }

                $contactMessageSlider->setAgreeDate(new \DateTime());
                $contactMessageSlider->setAgreeContent($this->renderView('ciamek/index/privacyPolicy.html.twig'));
                $em = $this->getDoctrine()->getManager();
                $em->persist($contactMessageSlider);
                $em->flush();

                $this->addFlash('alert-success', 'Zgłoszenie przyjęte');

                $contactMailer->sendEmail($contactMessageSlider);

                return $this->redirectToRoute('index');
            }
        } elseif ($formBottom->isSubmitted()) {
            if ($formBottom->isValid()) {
                /** @var UploadedFile $file */
                $file = $formBottom['file']->getData();
                if ($file) {
                    $originalFilename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
                    // this is needed to safely include the file name as part of the URL
                    $safeFilename = transliterator_transliterate('Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()', $originalFilename);
                    $newFilename = $safeFilename . '-' . uniqid() . '.' . $file->guessExtension();

                    // Move the file to the directory where brochures are stored
                    try {
                        $file->move(
                            $this->getParameter('kernel.project_dir') . '/uploads/badania',
                            $newFilename
                        );
                    } catch (FileException $e) {}

                    $contactMessageBottom->setFilename($newFilename);
                }

                $contactMessageBottom->setAgreeDate(new \DateTime());
                $contactMessageBottom->setAgreeContent($this->renderView('ciamek/index/privacyPolicy.html.twig'));
                $em = $this->getDoctrine()->getManager();
                $em->persist($contactMessageBottom);
                $em->flush();

                $this->addFlash('alert-success', 'Zgłoszenie przyjęte');

                $contactMailer->sendEmail($contactMessageBottom);

                return $this->redirectToRoute('index');
            }
        }

        return $this->render('ciamek/index/index.html.twig', [
            'formSlider' => $formSlider->createView(),
            'formBottom' => $formBottom->createView()
        ]);
    }

    public function contact(Request $request, ContactMailer $contactMailer) : Response
    {
        $contactMessage = new ContactMessage();
        $form = $this->createForm(ContactType::class, $contactMessage);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var UploadedFile $file */
            $file = $form['file']->getData();
            if ($file) {
                $originalFilename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
                // this is needed to safely include the file name as part of the URL
                $safeFilename = transliterator_transliterate('Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()', $originalFilename);
                $newFilename = $safeFilename.'-'.uniqid().'.'.$file->guessExtension();

                // Move the file to the directory where brochures are stored
                try {
                    $file->move(
                        $this->getParameter('kernel.project_dir') . '/uploads/badania',
                        $newFilename
                    );
                } catch (FileException $e) {}

                $contactMessage->setFilename($newFilename);
            }

            $contactMessage->setAgreeDate(new \DateTime());
            $contactMessage->setAgreeContent($this->renderView('ciamek/index/privacyPolicy.html.twig'));
            $em = $this->getDoctrine()->getManager();
            $em->persist($contactMessage);
            $em->flush();

            $this->addFlash('alert-success', 'Zgłoszenie przyjęte');

            $contactMailer->sendEmail($contactMessage);

            return $this->redirectToRoute('contact');
        }

        return $this->render('ciamek/index/contact.html.twig', [
            'form' => $form->createView()
        ]);
    }

    public function privacyPolicy() : Response
    {
        return $this->render('ciamek/index/privacyPolicy.html.twig');
    }
}