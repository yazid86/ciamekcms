<?php

namespace App\Controller;

use App\Entity\ContactMessage;
use App\Services\FileManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\File\MimeType\FileinfoMimeTypeGuesser;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class AdminMessagesController extends AbstractController
{
    public function index() : Response
    {
        $messages = $this->getDoctrine()->getManager()->getRepository(ContactMessage::class)->findBy([], ['agreeDate' => 'DESC']);

        return $this->render('admin/messages/index.html.twig', [
            'messages' => $messages,
            'current' => 'admin_messages'
        ]);
    }

    public function details($id, FileManager $fileManager) :Response
    {
        $message = $this->getDoctrine()->getManager()->getRepository(ContactMessage::class)->find((int)$id);

        if (!$message) {
            throw new NotFoundHttpException();
        }

        return $this->render('admin/messages/details.html.twig', [
            'message' => $message,
            'current' => 'admin_messages'
        ]);
    }

    public function open($id, FileManager $fileManager) :Response
    {
        $message = $this->getDoctrine()->getManager()->getRepository(ContactMessage::class)->find((int)$id);

        if (!$message) {
            throw new NotFoundHttpException();
        }

        return new BinaryFileResponse($fileManager->getFilePath($message->getFilename()));
    }

    public function download($id, FileManager $fileManager) :Response
    {
        $message = $this->getDoctrine()->getManager()->getRepository(ContactMessage::class)->find((int)$id);

        if (!$message) {
            throw new NotFoundHttpException();
        }

        $filename = $fileManager->getFilePath($message->getFilename());
        $response = new BinaryFileResponse($filename);
        // To generate a file download, you need the mimetype of the file
        $mimeTypeGuesser = new FileinfoMimeTypeGuesser();

        // Set the mimetype with the guesser or manually
        if($mimeTypeGuesser->isSupported()){
            // Guess the mimetype of the file according to the extension of the file
            $response->headers->set('Content-Type', $mimeTypeGuesser->guess($filename));
        }else{
            // Set the mimetype of the file manually, in this case for a text file is text/plain
            $response->headers->set('Content-Type', 'text/plain');
        }

        // Set content disposition inline of the file
        $response->setContentDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            $message->getFilename()
        );

        return $response;
    }


}