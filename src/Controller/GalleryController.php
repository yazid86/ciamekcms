<?php

namespace App\Controller;

use App\Entity\Gallery;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class GalleryController extends AbstractController
{
    public function index(Request $request) : Response
    {
	    $galleries = $this->getDoctrine()->getManager()->getRepository(Gallery::class)->findBy(['active' => true], ['name' => 'DESC']);
		if (count($galleries) == 0) {
			return $this->redirectToRoute('index');
		}

	    if (count($galleries) == 1) {
		    return $this->redirectToRoute('gallery_single', ['id' => $galleries[0]->getId()]);
	    }

        return $this->render('ciamek/gallery/index.html.twig', [
        	'galleries' => $galleries,
        ]);
    }

	public function single($id) :Response
	{
		$em = $this->getDoctrine()->getManager();
		$gallery = $em->getRepository(Gallery::class)->findOneBy(['id' => (int)$id, 'active' => true]);
		if (!$gallery) {
			throw new NotFoundHttpException('gallery not found');
		}

		return $this->render('ciamek/gallery/single.html.twig', [
			'gallery' => $gallery,
		]);
	}
}