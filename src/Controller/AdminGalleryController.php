<?php

namespace App\Controller;

use App\Entity\Gallery;
use App\Entity\Image;
use App\Form\Type\GalleryType;
use App\Services\FileManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\File\MimeType\FileinfoMimeTypeGuesser;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class AdminGalleryController extends AbstractController
{
    public function index() : Response
    {
        $galleries = $this->getDoctrine()->getManager()->getRepository(Gallery::class)->findBy([], ['name' => 'DESC']);

        return $this->render('admin/gallery/index.html.twig', [
            'galleries' => $galleries,
	        'current' => 'admin_gallery'
        ]);
    }

    public function form(Request $request, $id, FileManager $fileManager) :Response
    {
	    $em = $this->getDoctrine()->getManager();
	    if ($id) {
		    $gallery = $em->getRepository(Gallery::class)->find((int)$id);
		    if (!$gallery) {
			    throw new NotFoundHttpException('gallery not found');
		    }
	    } else {
		    $gallery = new Gallery();
	    }

	    $form = $this->createForm(GalleryType::class, $gallery);
	    $form->handleRequest($request);

	    if ($form->isSubmitted() && $form->isValid()) {
		    $em->persist($gallery);
		    $em->flush();

		    return $this->redirectToRoute('admin_gallery_details', ['id' => $gallery->getId()]);
	    }

        return $this->render('admin/gallery/form.html.twig', [
            'gallery' => $gallery,
            'current' => 'admin_gallery',
	        'form' => $form->createView()
        ]);
    }

    public function details($id, FileManager $fileManager) :Response
    {
	    $em = $this->getDoctrine()->getManager();
	    $gallery = $em->getRepository(Gallery::class)->find((int)$id);
	    if (!$gallery) {
		    throw new NotFoundHttpException('gallery not found');
	    }

        return $this->render('admin/gallery/details.html.twig', [
	        'current' => 'admin_gallery',
            'gallery' => $gallery,
	        'imagesCounter' => $em->getRepository(Image::class)->countByGallery($gallery),
        ]);
    }

    public function remove($id, FileManager $fileManager) :Response
    {
	    $em = $this->getDoctrine()->getManager();
	    $gallery = $em->getRepository(Gallery::class)->find((int)$id);
	    if (!$gallery) {
		    throw new NotFoundHttpException('gallery not found');
	    }

	    foreach ($gallery->getImages() as $image) {
		    unlink($this->getParameter('kernel.project_dir') . '/public/images/' . $image->getFilename());
		    $em->remove($image);
	    }

	    $em->remove($gallery);
	    $em->flush();

	    return new JsonResponse([], 200);
    }

//    public function open($id, FileManager $fileManager) :Response
//    {
//        $message = $this->getDoctrine()->getManager()->getRepository(ContactMessage::class)->find((int)$id);
//
//        if (!$message) {
//            throw new NotFoundHttpException();
//        }
//
//        return new BinaryFileResponse($fileManager->getFilePath($message->getFilename()));
//    }
//
//    public function download($id, FileManager $fileManager) :Response
//    {
//        $message = $this->getDoctrine()->getManager()->getRepository(ContactMessage::class)->find((int)$id);
//
//        if (!$message) {
//            throw new NotFoundHttpException();
//        }
//
//        $filename = $fileManager->getFilePath($message->getFilename());
//        $response = new BinaryFileResponse($filename);
//        // To generate a file download, you need the mimetype of the file
//        $mimeTypeGuesser = new FileinfoMimeTypeGuesser();
//
//        // Set the mimetype with the guesser or manually
//        if($mimeTypeGuesser->isSupported()){
//            // Guess the mimetype of the file according to the extension of the file
//            $response->headers->set('Content-Type', $mimeTypeGuesser->guess($filename));
//        }else{
//            // Set the mimetype of the file manually, in this case for a text file is text/plain
//            $response->headers->set('Content-Type', 'text/plain');
//        }
//
//        // Set content disposition inline of the file
//        $response->setContentDisposition(
//            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
//            $message->getFilename()
//        );
//
//        return $response;
//    }


}