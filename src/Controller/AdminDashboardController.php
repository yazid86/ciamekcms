<?php

namespace App\Controller;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class AdminDashboardController extends AbstractController
{
    public function index() : Response
    {
//        $user = $this->getDoctrine()->getManager()->getRepository(User::class)->find(1);
//        dump($user->getRoles());
//        exit;
        return $this->render('admin/dashboard.html.twig', ['current' => 'admin_index']);
    }
}