<?php

namespace App\Controller;

use App\Entity\Gallery;
use App\Entity\Image;
use App\Form\Type\ImageType;
use App\Services\FileManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\MimeType\FileinfoMimeTypeGuesser;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class AdminImageController extends AbstractController
{
    public function add(Request $request, $id, FileManager $fileManager) :Response
    {
	    $em = $this->getDoctrine()->getManager();
	    $gallery = $em->getRepository(Gallery::class)->find((int)$id);
	    if (!$gallery) {
		    throw new NotFoundHttpException('gallery not found');
	    }

	    $image = new Image();
	    $image->setGallery($gallery);
	    $form = $this->createForm(ImageType::class, $image);
	    $form->handleRequest($request);

	    if ($form->isSubmitted() && $form->isValid()) {
		    /** @var UploadedFile $file */
		    $file = $form['file']->getData();
		    if ($file) {
			    $originalFilename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
			    // this is needed to safely include the file name as part of the URL
//			    $safeFilename = transliterator_transliterate('Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()', $originalFilename);
			    $newFilename = uniqid() . '.' . $file->guessExtension();
			    $mime = $file->getMimeType();

			    // Move the file to the directory where brochures are stored
			    try {
				    $file->move(
					    $this->getParameter('kernel.project_dir') . '/public/images',
					    $newFilename
				    );
			    } catch (FileException $e) {}

			    $image->setFilename($newFilename);
			    $image->setMimeType($mime);
			    $image->setPath($newFilename);
		    }

		    $em = $this->getDoctrine()->getManager();
		    $em->persist($image);
		    $em->flush();

		    return $this->redirectToRoute('admin_gallery_details', ['id' => $gallery->getId()]);
	    }

        return $this->render('admin/image/add.html.twig', [
            'image' => $image,
            'current' => 'admin_gallery',
	        'form' => $form->createView()
        ]);
    }

	public function edit(Request $request, $id) :Response
	{
		$em = $this->getDoctrine()->getManager();
		$image = $em->getRepository( Image::class )->find( (int) $id );
		if ( ! $image ) {
			throw new NotFoundHttpException( 'gallery not found' );
		}

		$form = $this->createForm(ImageType::class, $image);
		$form->remove('file');
		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			$em = $this->getDoctrine()->getManager();
			$em->persist($image);
			$em->flush();

			return $this->redirectToRoute('admin_gallery_details', ['id' => $image->getGallery()->getId()]);
		}

		return $this->render('admin/image/edit.html.twig', [
			'image' => $image,
			'current' => 'admin_gallery',
			'form' => $form->createView()
		]);
	}

	public function remove(Request $request, $id) :Response
	{
		$em = $this->getDoctrine()->getManager();
		$image = $em->getRepository( Image::class )->find( (int) $id );
		if ( ! $image ) {
			throw new NotFoundHttpException( 'gallery not found' );
		}

		$galleryId = $image->getGallery()->getId();
		unlink($this->getParameter('kernel.project_dir') . '/public/images/' . $image->getFilename());
		$em->remove($image);
		$em->flush();

		return $this->redirectToRoute('admin_gallery_details', ['id' => $galleryId]);
	}
}