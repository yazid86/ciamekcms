<?php

namespace App\Services;

use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class ImageFileManager
{
    protected $parameterBag;
    protected $projectDir;

    public function __construct(ParameterBagInterface $parameterBag)
    {
        $this->parameterBag = $parameterBag;
        $this->projectDir = $parameterBag->get('kernel.project_dir');
    }

    public function getFilePath($name)
    {
        return $this->projectDir . '/public/images/'. $name;
    }
}