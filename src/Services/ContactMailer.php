<?php

namespace App\Services;

use App\Entity\ContactMessage;

class ContactMailer
{
    protected $mailer;
    protected $fileManager;

    public function __construct(\Swift_Mailer $mailer, FileManager $fileManager, $companyEmail)
    {
        $this->mailer = $mailer;
        $this->fileManager = $fileManager;
        $this->companyEmail = $companyEmail;
    }

    public function sendEmail(ContactMessage $contactMessage)
    {
        $message = (new \Swift_Message('Wiadomość od klienta'))
            ->setFrom('apprispl@gmail.com')
            ->setTo($this->companyEmail)
            ->setBody($contactMessage->getEmailContent(), 'text/html')
        ;

        if ($contactMessage->getEmail()) {
            $message->setReplyTo($contactMessage->getEmail());
        }

        if ($contactMessage->getFilename()) {
            $message->attach(\Swift_Attachment::fromPath($this->fileManager->getFilePath($contactMessage->getFilename())));
        }

        try {
            $this->mailer->send($message);
        } catch (\Exception $e) {
//            var_dump($e->getMessage());
//            exit;
        }
    }
}