<?php

namespace App\Form\Type;

use App\Entity\ContactMessage;
use App\Entity\Gallery;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class GalleryType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'required' => true,
	            'label' => 'name',
                'attr' => [
                    'placeholder' => 'enter_name'
                ],
	            'constraints' => [
	            	new NotBlank(),
		            new Length(['max' => 255])
	            ]
            ])
	        ->add('active', CheckboxType::class, [
	        	'label' => 'active_label',
	        	'required' => false,
	        ])
            ->add('save', SubmitType::class, ['label' => 'save'])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
        	'class' => Gallery::class
        ]);
    }


    public function getBlockPrefix()
    {
        return 'gallery_type';
    }


}