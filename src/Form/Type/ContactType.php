<?php

namespace App\Form\Type;

use App\Entity\ContactMessage;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Constraints\NotBlank;

class ContactType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName', TextType::class, [
                'required' => false,
                'attr' => [
                    'placeholder' => 'first_name'
                ]
            ])
            ->add('lastName', TextType::class, [
                'required' => false,
                'attr' => [
                    'placeholder' => 'last_name'
                ]
            ])
            ->add('email', EmailType::class, [
                'required' => true,
                'attr' => [
                    'placeholder' => 'email'
                ],
                'constraints' => [
                    new NotBlank()
                ]
            ])
            ->add('phone', TextType::class, [
                'required' => false,
                'attr' => [
                    'placeholder' => 'phone'
                ]
            ])
            ->add('subject', TextareaType::class, [
                'required' => false,
                'attr' => [
                    'placeholder' => 'message_subject'
                ]
            ])
            ->add('message', TextareaType::class, [
                'required' => true,
                'attr' => [
                    'placeholder' => 'what_can_we_help_you'
                ],
                'constraints' => [
                    new NotBlank()
                ]
            ])
            ->add('file', FileType::class, [
                'required' => false,
                'mapped' => false,
                'constraints' => [
                    new File([
                        'maxSize' => '10240k',
                        'mimeTypes' => [
                            'image/*',
                            'application/pdf',
                            'application/msword',
                            'application/vnd.ms-excel',
                            'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
                            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                            'application/vnd.openxmlformats-officedocument.presentationml.presentation',
                            'text/plain'

                        ],
                        'mimeTypesMessage' => 'Nieprawidłwy typ pliku'
                    ])
                ]
            ])
            ->add('agree', CheckboxType::class, [
                'label' => false
            ])
            ->add('send', SubmitType::class, ['label' => 'send'])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
//            'validation_groups' => function (FormInterface $form) {
//                /** @var ContactMessage $contactMessage */
//                $contactMessage = $form->getData();
//                $groups = ['Default'];
//
//                if ($contactMessage->getEmail() == '' && $contactMessage->getPhone() == '') {
//                    $groups = array_merge($groups, ['emailOrPhone']);
//                }
//
//                return $groups;
//            }
        ]);
    }


    public function getBlockPrefix()
    {
        return 'contact_type';
    }


}