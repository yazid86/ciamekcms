<?php

namespace App\Form\Type;

use App\Entity\Image;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class ImageType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
	        ->add('alt', TextType::class, [
	        	'required' => false,
		        'label' => 'alt_label'
	        ])
	        ->add('description', TextType::class, [
	        	'required' => false,
		        'label' => 'description',
		        'constraints' => [
		        	new Length(['max' => 255]),
		        ]
	        ])
	        ->add('file', FileType::class, [
		        'required' => false,
		        'mapped' => false,
		        'constraints' => [
			        new File([
				        'maxSize' => '10240k',
				        'mimeTypes' => [
					        'image/*',
				        ],
				        'mimeTypesMessage' => 'Nieprawidłwy typ pliku'
			        ])
		        ]
	        ])
	        ->add('save', SubmitType::class, ['label' => 'save'])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
	        'class' => Image::class
        ]);
    }


    public function getBlockPrefix()
    {
        return 'image_type';
    }


}