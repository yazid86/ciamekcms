<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
// * @ORM\Entity(repositoryClass="App\Repository\ContactMessageRepository")
 * @ORM\Entity()
 * @ORM\Table(name="contact_message")
 */
class ContactMessage
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $firstName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $lastName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $phone;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $subject;

    /**
     * @ORM\Column(type="text", nullable=false)
     */
    private $message;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $filename;

    /**
     * @ORM\Column(type="boolean", nullable=false)
     */
    private $agree = false;

    /**
     * @ORM\Column(type="text", nullable=false)
     */
    private $agreeContent;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $agreeDate;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFirstName()
    {
        return $this->firstName;
    }

    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName()
    {
        return $this->lastName;
    }

    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * An email that represents this user.
     *
     * @see UserInterface
     */
    public function getEmail(): string
    {
        return (string)$this->email;
    }

    public function setEmail(string $email = null): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPhone(): string
    {
        return (string)$this->phone;
    }

    public function setPhone(string $phone = null): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getSubject()
    {
        return $this->subject;
    }

    public function setSubject($subject): self
    {
        $this->subject = $subject;

        return $this;
    }

    public function getMessage()
    {
        return $this->message;
    }

    public function setMessage($message): self
    {
        $this->message = $message;

        return $this;
    }

    public function getFilename()
    {
        return $this->filename;
    }

    public function setFilename($filename): self
    {
        $this->filename = $filename;

        return $this;
    }

    public function getAgree()
    {
        return $this->agree;
    }

    public function setAgree($agree): self
    {
        $this->agree = $agree;

        return $this;
    }

    public function getAgreeContent()
    {
        return $this->agreeContent;
    }

    public function setAgreeContent($agreeContent): self
    {
        $this->agreeContent = $agreeContent;

        return $this;
    }

    public function getAgreeDate()
    {
        return $this->agreeDate;
    }

    public function setAgreeDate($agreeDate): self
    {
        $this->agreeDate = $agreeDate;

        return $this;
    }

    public function getEmailContent()
    {
        $body = 'Wiadomość od klienta: <br>';
        if ($this->getFirstName()) {
            $body .= '<br>Imię: ' . $this->getFirstName();
        }

        if ($this->getLastName()) {
            $body .= '<br>Nazwisko: ' . $this->getLastName();
        }

        if ($this->getEmail()) {
            $body .= '<br>Email: ' . $this->getEmail();
        }

        if ($this->getPhone()) {
            $body .= '<br>Telefon: ' . $this->getPhone();
        }

        if ($this->getSubject()) {
            $body .= '<br>Temat: ' . $this->getSubject();
        }

        if ($this->getMessage()) {
            $body .= '<br>Wiadomość:<br>' . $this->getMessage();
        }

        return $body;
    }
}
