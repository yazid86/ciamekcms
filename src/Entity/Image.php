<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
// * @ORM\Entity(repositoryClass="App\Repository\ImageRepository")
 * @ORM\Entity()
 * @ORM\Table(name="image")
 */
class Image
{
	use TimestampableEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

	/**
	 * @ORM\Column(type="string", length=255, nullable=false)
	 */
	private $filename;

	/**
	 * @ORM\Column(type="string", length=255, nullable=false)
	 */
	private $path;

	/**
	 * @ORM\Column(type="string", length=255, nullable=false)
	 */
	private $mime;

	/**
	 * @ORM\Column(type="string", length=255, nullable=true)
	 */
	private $alt;

	/**
	 * @ORM\Column(type="string", length=255, nullable=true)
	 */
	private $description;

	/**
	 * @Doctrine\ORM\Mapping\ManyToOne(targetEntity="Gallery")
	 * @Doctrine\ORM\Mapping\JoinColumn(name="gallery_id", referencedColumnName="id")
	 */
	private $gallery;

	public function getId()
	{
		return $this->id;
	}

	public function getFilename()
	{
		return $this->filename;
	}

	public function setFilename($filename): self
	{
		$this->filename = $filename;

		return $this;
	}

	public function getPath()
	{
		return $this->path;
	}

	public function setPath($path): self
	{
		$this->path = $path;

		return $this;
	}

	public function getMime()
	{
		return $this->mime;
	}

	public function setMimeType($mime): self
	{
		$this->mime = $mime;

		return $this;
	}

	public function setAlt($alt) : self
	{
		$this->alt = $alt;

		return $this;
	}

	public function getAlt()
	{
		return $this->alt;
	}

	public function setDescription($description) : self
	{
		$this->description = $description;

		return $this;
	}

	public function getDescription()
	{
		return $this->description;
	}

	public function getGallery(): Gallery
	{
		return $this->gallery;
	}

	public function setGallery(Gallery $gallery): self
	{
		$this->gallery = $gallery;

		return $this;
	}
}
