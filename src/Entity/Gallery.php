<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
// * @ORM\Entity(repositoryClass="App\Repository\GalleryRepository")
 * @ORM\Entity()
 * @ORM\Table(name="gallery")
 */
class Gallery
{
	use TimestampableEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

	/**
	 * @ORM\Column(type="string", length=255, nullable=false)
	 */
	private $name;

	/**
	 * @ORM\Column(type="boolean", length=255, nullable=false, options={"default" : 1})
	 */
	private $active = true;

	/**
	 * @Doctrine\ORM\Mapping\OneToMany(targetEntity="App\Entity\Image", mappedBy="gallery", cascade={"persist", "remove"})
	 */
	private $images;

	public function __construct()
	{
		$this->images = new ArrayCollection();
	}

	public function getId()
	{
		return $this->id;
	}

	public function getName()
	{
		return $this->name;
	}

	public function setName($name): self
	{
		$this->name = $name;

		return $this;
	}

	public function setActive($active) : self
	{
		$this->active = $active;

		return $this;
	}

	public function getActive() : bool
	{
		return $this->active;
	}

	public function getImages()
	{
		return $this->images;
	}

	public function addImage(Image $image)
	{
		$this->images->add($image);

		return $this;
	}

	public function removeImage(Image $image)
	{
		$this->images->removeElement($image);

		return $this;
	}

	public function getMainImage()
	{
		if (count($this->getImages()) > 0) {
			return $this->getImages()->current();
		}

		return null;
	}
}
